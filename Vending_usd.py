import emoji, os, sys, termios, tty


class VendingMachine():

    def __init__(self, nickels=10, dimes=10, quarters=10, cola={'num': 10, 'price': 100},
                 crisps={'num': 10, 'price': 50}, choc={'num': 10, 'price': 65}):
        self.nickels = nickels
        self.dimes = dimes
        self.quarters = quarters
        self.cola = cola
        self.crisps = crisps
        self.choc = choc
        self.user_money = 0

    def accept_coins(self, value):
        if value not in [1, 2, 3]:
            return 'Not Valid!'
        if value == 1:
            self.user_money += 5
            self.nickels += 1
        if value == 2:
            self.user_money += 10
            self.dimes += 1
        if value == 3:
            self.user_money += 25
            self.quarters += 1

    def select_product(self, product):
        # valid product check
        if product not in ['a', 'b', 'c']:
            return 'Please select the valid product'
        name_dict = {
            'a': 'cola',
            'b': 'crisps',
            'c': 'choc'
        }
        product_dict = {
            'name': name_dict[product],
        }
        # stock check
        if self.__dict__[product_dict['name']]['num'] == 0:
            return 'Out of stock'
        # return product
        return product_dict

    def make_change(self, product):
        name_dict = {
            'a': 'cola',
            'b': 'crisps',
            'c': 'choc'
        }
        total_money = self.user_money
        product_price = self.__dict__[name_dict['name']]['price']
        if total_money < product:
            return 'Insert coin'
        return total_money - product_price

    def get_return_coins(self, change):
        coins = {"quarters": 0, "dimes": 0, "nickels": 0}
        coins['quarters'] = int(min(self.quarters, change//25))
        change -= coins['quarters'] * 25
        coins['dimes'] = int(min(self.dimes, change//10))
        change -= coins['dimes'] * 10
        coins['nickels'] = int(min(self.nickels, change // 5))
        change -= coins['nickels'] * 5
        # print(change)
        return change, coins

    def return_coins(self):
        value_dict = {
            'quarters': 25,
            'dimes': 10,
            'nickels': 5
        }
        change = self.user_money
        _, coins_dict = self.get_return_coins(change)
        for key in coins_dict:
            self.__dict__[key] -= coins_dict[key]
            self.user_money -= value_dict[key] * coins_dict[key]
        return coins_dict

    def get_product_status(self, product_name):
        total_money = self.user_money
        num = self.__dict__[product_name]['num']
        change = total_money - self.__dict__[product_name]['price']
        can_buy = (change >= 0)
        change_left, coins = self.get_return_coins(change)
        # print('changes for {0} is {1}'.format(product_name, change),end='\r\n')
        exact_change = (change_left > 0)
        return num, can_buy, exact_change

    def buy_product(self, product):
        name_dict = {
            'a': 'cola',
            'b': 'crisps',
            'c': 'choc'
        }
        product_name = name_dict[product]
        num, can_buy, _ = self.get_product_status(product_name)
        if num == 0:
            return 'Out of stock'
        if not can_buy:
            return 'Insert coins'
        self.__dict__[product_name]['num'] -= 1
        price =  self.__dict__[product_name]['price']
        self.user_money -= price
        return True

    def show_infor(self):
        os.system('clear')
        total_money = self.user_money
        prodcut = ['cola', 'crisps', 'choc']
        nums = []
        can_buys = []
        exact_changes = []
        for item in prodcut:
            num, can_buy, exact_change = self.get_product_status(item)
            nums.append(num)
            can_buys.append(can_buy)
            exact_changes.append(exact_change)

        print(" " * 4,'Welcome to use the Vending Machine Alpha', end='\r\n')
        print(" " * 6, 'Cola', " " * 6, 'Crisps', ' ' * 6, 'Chocolate', end='\r\n')
        print(" " * 7, emoji.emojize(':cup_with_straw:'), " " * 8,
              emoji.emojize(' :fish_cake_with_swirl:'), " " * 11,
              emoji.emojize(' :chocolate_bar:'), end='\r\n'
              )
        print(" " * 5, '{} left'.format(nums[0]), ' ' * 4,
              '{} left'.format(nums[1]), ' ' * 6,
              '{} left'.format(nums[2]), end='\r\n')
        print(" " * 4, 'price: 100   price: 50      price: 65', end='\r\n')
        exact_check = all(exact_changes)
        if exact_check:
            print(' ' * 4, 'EXACT CHANGE ONLY', end='\r\n')
        # elif total_money == 0:
        #     print(' ' * 5, 'INSERT COINS', end='\r\n')
        if total_money >= 0:
            dollar_sign = emoji.emojize(':dollar_banknote:')
            print('Money insert{0}: {1}'.format(dollar_sign, total_money), end='\r\n')

        print('Select [a] cola, [b] crisps or [c] for Chocolate', end='\r\n')
        print('Insert [1] nickel, [2] dime or [3] quarter', end='\r\n')
        print('Press [r] to have coins returned or [q] to quit', end='\r\n')


if __name__ == '__main__':
    vending = VendingMachine()
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    tty.setraw(fd)
    msg = ''
    try:
        while True:
            vending.show_infor()
            if isinstance(msg, str) and len(msg) > 0:
                print(msg, end='\r\n')
                msg =''
            ch = sys.stdin.read(1)
            if ch in ['1', '2', '3']:
                vending.accept_coins(int(ch))
            if ch in ['a', 'b', 'c']:
                msg = vending.buy_product(ch)

            if ch == 'r':
                dict = vending.return_coins()
                msg = 'Here is the change nickels:{0}, dimes:{1} and quarters:{2}'.format(
                    dict['nickels'], dict['dimes'], dict['quarters']
                )
            if ch == 'q':
                sys.exit(0)
    finally:
        termios.tcsetattr(fd, termios.TCSANOW, old_settings)