import emoji, os, sys, termios, tty


class VendingMachine():

    def __init__(self, coins_5=10, coins_10=10, coins_20=10, coins_50=10, coins_100=10, coins_200=10,
                 cola={'num': 10, 'price': 100}, crisps={'num': 10, 'price': 50}, choc={'num': 10, 'price': 50}):
        self.coins_5 = coins_5
        self.coins_10 = coins_10
        self.coins_20 = coins_20
        self.coins_50 = coins_50
        self.coins_100 = coins_100
        self.coins_200 = coins_200
        self.cola = cola
        self.crisps = crisps
        self.choc = choc
        self.user_money = 0

    def accept_coins(self, value):
        if value not in [1, 2, 3, 4, 5, 6]:
            return 'Not Valid!'
        if value == 1:
            self.user_money += 5
            self.coins_5 += 1
        if value == 2:
            self.user_money += 10
            self.coins_10 += 1
        if value == 3:
            self.user_money += 20
            self.coins_20 += 1
        if value == 4:
            self.user_money += 50
            self.coins_50 += 1
        if value == 5:
            self.user_money += 100
            self.coins_100 += 1
        if value == 6:
            self.user_money += 200
            self.coins_200 += 1

    def select_product(self, product):
        # valid product check
        if product not in ['a', 'b', 'c']:
            return 'Please select the valid product'
        name_dict = {
            'a': 'cola',
            'b': 'crisps',
            'c': 'choc'
        }
        product_dict = {
            'name': name_dict[product],
        }
        # stock check
        if self.__dict__[product_dict['name']]['num'] == 0:
            return 'Out of stock'
        # return product
        return product_dict

    def make_change(self, product):
        name_dict = {
            'a': 'cola',
            'b': 'crisps',
            'c': 'choc'
        }
        total_money = self.user_money
        product_price = self.__dict__[name_dict['name']]['price']
        if total_money < product:
            return 'Insert coin'
        return total_money - product_price

    def get_return_coins(self, change):
        coins = {"5p": 0, "10p": 0, "20p": 0, '50p':0, '£1': 0, '£2': 0}
        coins['£2'] = int(min(self.coins_200, change//200))
        change -= coins['£2'] * 200
        coins['£1'] = int(min(self.coins_100, change // 100))
        change -= coins['£1'] * 100
        for i in [50, 20, 10, 5]:
            coins[str(i)+'p'] = int(min(self.__dict__['coins_'+str(i)], change // i))
            change -= coins[str(i)+'p'] * i
        # print(change)
        return change, coins

    def return_coins(self):
        value_dict = {
            '£2': 200,
            '£1': 100,
            '50p': 50,
            '20p': 20,
            '10p': 10,
            '5p': 5
        }
        map_dict = {
            '£2': 'coins_200',
            '£1': 'coins_100',
            '50p': 'coins_50',
            '20p': 'coins_20',
            '10p': 'coins_10',
            '5p': 'coins_5'
        }
        change = self.user_money
        _, coins_dict = self.get_return_coins(change)
        for key in coins_dict:
            self.__dict__[map_dict[key]] -= coins_dict[key]
            self.user_money -= value_dict[key] * coins_dict[key]
        return coins_dict

    def get_product_status(self, product_name):
        total_money = self.user_money
        num = self.__dict__[product_name]['num']
        change = total_money - self.__dict__[product_name]['price']
        can_buy = (change >= 0)
        change_left, coins = self.get_return_coins(change)
        # print('changes for {0} is {1}'.format(product_name, change),end='\r\n')
        exact_change = (change_left > 0)
        return num, can_buy, exact_change

    def buy_product(self, product):
        name_dict = {
            'a': 'cola',
            'b': 'crisps',
            'c': 'choc'
        }
        product_name = name_dict[product]
        num, can_buy, _ = self.get_product_status(product_name)
        if num == 0:
            return 'Out of stock'
        if not can_buy:
            return 'Insert coins'
        self.__dict__[product_name]['num'] -= 1
        price =  self.__dict__[product_name]['price']
        self.user_money -= price
        return True

    def show_infor(self):
        os.system('clear')
        total_money = self.user_money
        prodcut = ['cola', 'crisps', 'choc']
        nums = []
        can_buys = []
        exact_changes = []
        for item in prodcut:
            num, can_buy, exact_change = self.get_product_status(item)
            nums.append(num)
            can_buys.append(can_buy)
            exact_changes.append(exact_change)

        print(" " * 4,'Welcome to use the Vending Machine Alpha', end='\r\n')
        print(" " * 6, 'Cola', " " * 6, 'Crisps', ' ' * 6, 'Chocolate', end='\r\n')
        print(" " * 7, emoji.emojize(':cup_with_straw:'), " " * 8,
              emoji.emojize(' :fish_cake_with_swirl:'), " " * 11,
              emoji.emojize(' :chocolate_bar:'), end='\r\n'
              )
        print(" " * 5, '{} left'.format(nums[0]), ' ' * 4,
              '{} left'.format(nums[1]), ' ' * 6,
              '{} left'.format(nums[2]), end='\r\n')
        print(" " * 4, 'price: 100   price: 50      price: 65', end='\r\n')
        exact_check = all(exact_changes)
        if exact_check:
            print(' ' * 4, 'EXACT CHANGE ONLY', end='\r\n')
        # elif total_money == 0:
        #     print(' ' * 5, 'INSERT COINS', end='\r\n')
        if total_money >= 0:
            dollar_sign = emoji.emojize(':dollar_banknote:')
            print('Money insert{0}: {1}'.format(dollar_sign, total_money), end='\r\n')

        print('Select [a] cola, [b] crisps or [c] for Chocolate', end='\r\n')
        print('Insert [1] 5p, [2] 10p, [3] 20p, [4] 50p, [5] £1 or [6] £2', end='\r\n')
        print('Press [r] to have coins returned or [q] to quit', end='\r\n')


if __name__ == '__main__':
    vending = VendingMachine()
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    tty.setraw(fd)
    msg = ''
    try:
        while True:
            vending.show_infor()
            if isinstance(msg, str) and len(msg) > 0:
                print(msg, end='\r\n')
                msg =''
            ch = sys.stdin.read(1)
            if ch in ['1', '2', '3', '4', '5', '6']:
                vending.accept_coins(int(ch))
            if ch in ['a', 'b', 'c']:
                msg = vending.buy_product(ch)

            if ch == 'r':
                dict = vending.return_coins()
                msg = 'Here is the change'
                for key in dict:
                    msg += key + ': {} '.format(dict[key])
            if ch == 'q':
                sys.exit(0)
    finally:
        termios.tcsetattr(fd, termios.TCSANOW, old_settings)